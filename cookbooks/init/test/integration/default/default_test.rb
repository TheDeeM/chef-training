describe file('/etc/chef/client.rb') do
  it { should exist }
  its('content') { should match /chef_server_url/ }
end

describe file('/root/.chef/config.rb') do
  it { should exist }
  its('content') { should match /chef_server_url/ }
end
