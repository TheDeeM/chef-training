directory 'Make sure directory for chef-client config exists' do
    path       ::File.dirname(node['init']['config']['chef-client']['path'])
    recursive  true
    action     :create
end

template 'Create config for chef-client' do
    source    'client.rb.erb'
    path      node['init']['config']['chef-client']['path']
    variables ({
       :ORGANIZATION_NAME => node['init']['server']['organization'],
       :SERVER_PORT       => node['init']['server']['port'],
       :SERVER_HOST       => node['init']['server']['hostname']
    })
    action     :create
end

directory 'Make sure directory for knife config exists' do
  path       ::File.dirname(node['init']['config']['knife']['path'])
  recursive  true
  action     :create
end

template 'Create config for knife' do
    source    'client.rb.erb'
    path      node['init']['config']['knife']['path']
    variables ({
        :ORGANIZATION_NAME => node['init']['server']['organization'],
        :SERVER_PORT       => node['init']['server']['port'],
        :SERVER_HOST       => node['init']['server']['hostname'],
        :CONFIGURE_COOKBOOK_PATH => true
    })
    action     :create
end