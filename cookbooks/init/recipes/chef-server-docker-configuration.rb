directory 'Make sure directory for knife config exists' do
  path       node['init']['project_path']
  recursive  true
  action     :create
end

template 'Create config for knife' do
  source    'docker-compose.yml.erb'
  path      "#{node['init']['project_path']}/docker-compose.yml"
  variables ({
      :ORGANIZATION_NAME => node['init']['server']['organization'],
      :SERVER_PORT       => node['init']['server']['port']
  })
  action     :create
end