default['init']['config']['chef-client']['path'] = Chef::Platform.windows? ? 'C:/chef/client.rb' : '/etc/chef/client.rb'
default['init']['config']['knife']['path']       = "#{Dir.home}/.chef/config.rb"

default['init']['server']['port']                = 443
default['init']['server']['organization']        = 'szkolenie'
default['init']['server']['hostname']            = 'localhost'

default['init']['project_path']                  = Chef::Platform.windows? ? 'C:/Szkolenie/Chef' : "#{Dir.home}/szkolenie/chef"