filename = 'C:/plik.txt'

file filename do
  content   'content1'
end

file filename do
  content   'content'
  not_if    {::File.exist?(filename)}
end

file filename do
  only_if    {::File.exist?(filename)}
  action    :delete
end