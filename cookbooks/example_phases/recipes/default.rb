filename = 'C:/plik.txt'

file filename do
  content   'content1'
end

file_exist = ::File.exist?(filename)

file filename do
  content   'content2'
  not_if    {file_exist}
end

file_exist2 = ::File.exist?(filename)

file filename do
  only_if    {file_exist2}
  action    :delete
end