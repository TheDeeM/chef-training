#!/bin/bash
set -e

# First start the Chef server
init.sh

# Configure our cookbooks
cd /home/chef/server/utils/
berks vendor cookbooks
cd cookbooks
knife upload *

tail -F /opt/opscode/embedded/service/*/log/current