@echo off

echo "Configuring chef client..."
docker cp chef-server:/home/chef/server/certificates C:/chef

echo "Configuring knife..."
docker cp chef-server:/home/chef/server/certificates C:/Users/%USERNAME%/.chef

echo "Configuring done!"
pause