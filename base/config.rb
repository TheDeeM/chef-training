log_level          :info
log_location       STDOUT
ssl_verify_mode    :verify_none
chef_server_url    "https://localhost:#{ENV['SERVER_PORT']}/organizations/#{ENV['ORGANIZATION_NAME']}"

validation_client_name 'szkolenie-validator'
validation_key         "#{ENV['CERTIFICATES_DIR']}/#{ENV['ORGANIZATION_NAME']}-validator.pem"
client_key             "#{ENV['CERTIFICATES_DIR']}/#{ENV['USER_NAME']}.pem"
node_name              ENV['USER_NAME']

cookbook_path ['.', '..']

Mixlib::Log::Formatter.show_time = true