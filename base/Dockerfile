FROM ubuntu:14.04

WORKDIR /home/chef/server

# Install dependencies
RUN apt-get update \
	&& apt-get -y install wget iproute2 ohai \
	&& apt-get clean 
	
# Install clean Chef server
RUN wget -O chef-server-core.deb  https://packages.chef.io/files/stable/chef-server/12.18.14/ubuntu/14.04/chef-server-core_12.18.14-1_amd64.deb \
	&& dpkg -i chef-server-core.deb \
	&& rm -f chef-server-core.deb \
	# Install ChefDK for administrators
	&& wget -O chefdk.deb https://packages.chef.io/files/stable/chefdk/3.4.38/ubuntu/14.04/chefdk_3.4.38-1_amd64.deb \
	&& dpkg -i chefdk.deb \
	&& rm -f chefdk.deb

# Configure the Chef server
ENV CERTIFICATES_DIR=/home/chef/server/certificates \
	USER_NAME=admin \
	USER_PASS=admin123 \
	USER_EMAIL=thedeemling@gmail.com \
	ORGANIZATION_NAME=training \
	SERVER_PORT=443

EXPOSE 443
#VOLUME ["/etc/opscode", "/var/opt/opscode"]

# We need to set-up interfaces, otherwise ohai will not list them: https://discourse.chef.io/t/ohai-not-picking-up-any-interfaces-when-an-ipv6-gateway-ending-in-is-used/8070/5
RUN ip -f inet6 neigh show \
	&& ip -o -f inet6 route show table main \
	&& ip -d -s link  \
	&& mkdir -p $CERTIFICATES_DIR

COPY bin /usr/local/bin
COPY config.rb /root/.chef/config.rb

CMD ["/bin/bash", "run.sh"]