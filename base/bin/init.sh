#!/bin/bash
set -e

HOSTNAME="$(hostname -f)"
echo "api_fqdn \"${HOSTNAME}\""                >  /etc/opscode/chef-server.rb
echo "nginx['ssl_port'] = ${SERVER_PORT}"      >> /etc/opscode/chef-server.rb
echo "bookshelf['vip_port'] = ${SERVER_PORT}"  >> /etc/opscode/chef-server.rb

sysctl -wq kernel.shmmax=17179869184 
sysctl -wq net.ipv6.conf.lo.disable_ipv6=0 

/opt/opscode/embedded/bin/runsvdir-start &
chef-server-ctl reconfigure

create-user.sh "${USER_NAME}" "${USER_EMAIL}" "${USER_PASS}" "${ORGANIZATION_NAME}"