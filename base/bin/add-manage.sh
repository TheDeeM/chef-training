#!/bin/bash

set -e

chef-server-ctl install chef-manage
(/opt/opscode/embedded/bin/runsvdir-start &) && chef-server-ctl reconfigure
(/opt/opscode/embedded/bin/runsvdir-start &) && chef-manage-ctl reconfigure --accept-license