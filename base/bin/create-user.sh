#!/bin/bash

set -e

USER_NAME=$1
EMAIL=$2
PASSWORD=$3
ORGANIZATION=$4

chef-server-ctl user-create ${USER_NAME} ${USER_NAME} ${USER_NAME} ${EMAIL} "${PASSWORD}" --filename "${CERTIFICATES_DIR}/${USER_NAME}.pem"
chef-server-ctl org-create ${ORGANIZATION} "${ORGANIZATION}-test" --association_user ${USER_NAME} --filename "${CERTIFICATES_DIR}/${ORGANIZATION}-validator.pem"